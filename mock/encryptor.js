import Mock from 'mockjs'

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    hsmIp: '@ip()',
    hsmPort: '@integer(1000, 9999)',
    hsmFactory: '@pick(["xxx高科科技发展有限公司", "xxx加密机共享有限责任公司", "xxx信息技术有限公司", "xxx电子科技有限公司","xxx科技发展有限公司","xxx通信技术有限公司"])',
    hsmModel: '@integer(0, 4)',
    hsmVersion: '@pick(["H1.01.18", "H2.03.12", "M1.16.01", "M8.18.08"])',
    hsmName: '@integer(0, 1)',
    hsmUse: '@pick(["国密一发", "互联网空充", "SE发行"])',
    hsmState: '@integer(0, 2)',
    remarks: '',
    applyOperid: '@cname',
    applyTime: '@datetime',
    approveOperid: '@cname',
    approveTime: '@datetime'
  }))
}

export default [
  {
    url: '/encryptors/page',
    type: 'get',
    response: config => {
      const { hsmName, hsmState, hsmModel, page = 1, limit = 10, sort } = config.query

      let mockList = List.filter(item => {
        if (hsmModel && item.hsmModel !== parseInt(hsmModel)) return false
        if (hsmState && item.hsmState !== parseInt(hsmState)) return false
        if (hsmName && item.hsmName.indexOf(hsmName) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/encryptors/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const encryptor of List) {
        if (encryptor.id === +id) {
          return {
            code: 200,
            success: true,
            data: encryptor
          }
        }
      }
    }
  },

  {
    url: '/encryptors',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/encryptors',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]

