// 模拟菜单数据
export const menuData = [
  {
    id: 1000,
    parentId: undefined,
    name: '加密机管理',
    perms: 'encryptor',
    url: 'encryptor/index',
    icon: 'encryptor',
    type: 1,
    status: 1,
    orderNum: 1,
    children: [
      { id: 1001, parentId: 1000, name: '新增', perms: 'encryptor:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
      { id: 1002, parentId: 1000, name: '删除', perms: 'encryptor:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
      { id: 1003, parentId: 1000, name: '修改', perms: 'encryptor:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 },
      { id: 1004, parentId: 1000, name: '审批', perms: 'encryptor:approve', icon: 'el-icon-finished', type: 2, status: 1, orderNum: 1 },
      { id: 1005, parentId: 1000, name: '上架', perms: 'encryptor:up', icon: 'el-icon-top', type: 2, status: 1, orderNum: 1 },
      { id: 1006, parentId: 1000, name: '下架', perms: 'encryptor:down', icon: 'el-icon-bottom', type: 2, status: 1, orderNum: 1 }
    ]
  },
  {
    id: 1100,
    parentId: undefined,
    name: '密钥管理',
    perms: 'secretKey',
    url: 'secretKey/index',
    icon: 'password',
    type: 0,
    status: 1,
    orderNum: 2,
    children: [
      {
        id: 1101,
        parentId: 1100,
        name: '对称密钥管理',
        perms: 'symmetricKey',
        url: 'symmetricKey/index',
        icon: 'symmetricKey',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1102, parentId: 1101, name: '新增', perms: 'symmetricKey:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1103, parentId: 1101, name: '删除', perms: 'symmetricKey:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1104, parentId: 1101, name: '修改', perms: 'symmetricKey:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 },
          { id: 1105, parentId: 1101, name: '审批', perms: 'symmetricKey:approve', icon: 'el-icon-finished', type: 2, status: 1, orderNum: 1 },
          { id: 1106, parentId: 1101, name: '解冻', perms: 'symmetricKey:unfreeze', icon: 'el-icon-top-left', type: 2, status: 1, orderNum: 1 }
        ]
      },
      {
        id: 1107,
        parentId: 1100,
        name: '非对称密钥管理',
        perms: 'asymmetricKey',
        url: 'asymmetricKey/index',
        icon: 'asymmetricKey',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1108, parentId: 1107, name: '新增', perms: 'asymmetricKey:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1109, parentId: 1107, name: '删除', perms: 'asymmetricKey:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1110, parentId: 1107, name: '修改', perms: 'asymmetricKey:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 },
          { id: 1111, parentId: 1107, name: '审批', perms: 'asymmetricKey:approve', icon: 'el-icon-finished', type: 2, status: 1, orderNum: 1 },
          { id: 1112, parentId: 1107, name: '解冻', perms: 'asymmetricKey:unfreeze', icon: 'el-icon-top-left', type: 2, status: 1, orderNum: 1 }
        ]
      }
    ]
  },
  {
    id: 1200,
    parentId: undefined,
    name: '证书管理',
    perms: 'certificate',
    url: 'certificate/index',
    icon: 'certificate',
    type: 0,
    status: 1,
    orderNum: 2,
    children: [
      {
        id: 1201,
        parentId: 1200,
        name: '证书服务配置管理',
        perms: 'certConfig',
        url: 'certConfig/index',
        icon: 'certConfig',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1202, parentId: 1201, name: '新增', perms: 'certConfig:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1203, parentId: 1201, name: '删除', perms: 'certConfig:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1204, parentId: 1201, name: '修改', perms: 'certConfig:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 },
          { id: 1205, parentId: 1201, name: '审批', perms: 'certConfig:approve', icon: 'el-icon-finished', type: 2, status: 1, orderNum: 1 },
          { id: 1206, parentId: 1201, name: '停止', perms: 'certConfig:stop', icon: 'el-icon-switch-button', type: 2, status: 1, orderNum: 1 }
        ]
      },
      {
        id: 1207,
        parentId: 1200,
        name: '证书签发管理',
        perms: 'certIssuance',
        url: 'certIssuance/index',
        icon: 'certIssuance',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1208, parentId: 1207, name: '新增', perms: 'certIssuance:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1209, parentId: 1207, name: '删除', perms: 'certIssuance:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1210, parentId: 1207, name: '修改', perms: 'certIssuance:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 },
          { id: 1211, parentId: 1207, name: '审批', perms: 'certIssuance:approve', icon: 'el-icon-finished', type: 2, status: 1, orderNum: 1 },
          { id: 1212, parentId: 1207, name: '解冻', perms: 'certIssuance:unfreeze', icon: 'el-icon-top-left', type: 2, status: 1, orderNum: 1 }
        ]
      }
    ]
  },
  {
    id: 1300,
    parentId: undefined,
    name: '监控管理',
    perms: 'monitor',
    url: 'monitor/index',
    icon: 'monitor',
    type: 1,
    status: 1,
    orderNum: 1,
    children: [
      { id: 1301, parentId: 1300, name: '新增', perms: 'monitor:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
      { id: 1302, parentId: 1300, name: '删除', perms: 'monitor:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
      { id: 1303, parentId: 1300, name: '修改', perms: 'monitor:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 }
    ]
  },
  {
    id: 1400,
    parentId: undefined,
    name: '系统管理',
    perms: 'system',
    url: 'system/index',
    icon: 'system',
    type: 0,
    status: 1,
    orderNum: 2,
    children: [
      {
        id: 1401,
        parentId: 1400,
        name: '用户管理',
        perms: 'user',
        url: 'user/index',
        icon: 'user',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1402, parentId: 1401, name: '新增', perms: 'user:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1403, parentId: 1401, name: '删除', perms: 'user:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1404, parentId: 1401, name: '修改', perms: 'user:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 }
        ]
      },
      {
        id: 1407,
        parentId: 1400,
        name: '角色管理',
        perms: 'role',
        url: 'role/index',
        icon: 'role',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1408, parentId: 1407, name: '新增', perms: 'role:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1409, parentId: 1407, name: '删除', perms: 'role:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1410, parentId: 1407, name: '修改', perms: 'role:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 }
        ]
      },
      {
        id: 1411,
        parentId: 1400,
        name: '菜单管理',
        perms: 'menu',
        url: 'menu/index',
        icon: 'list',
        type: 1,
        status: 1,
        orderNum: 1,
        children: [
          { id: 1412, parentId: 1411, name: '新增', perms: 'menu:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
          { id: 1413, parentId: 1411, name: '删除', perms: 'menu:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
          { id: 1414, parentId: 1411, name: '修改', perms: 'menu:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 }
        ]
      }
    ]
  },
  {
    id: 1500,
    parentId: undefined,
    name: 'UKEY管理',
    perms: 'ukey',
    url: 'ukey/index',
    icon: 'ukey',
    type: 1,
    status: 1,
    orderNum: 1,
    children: [
      { id: 1501, parentId: 1500, name: '新增', perms: 'ukey:add', icon: 'el-icon-plus', type: 2, status: 1, orderNum: 1 },
      { id: 1502, parentId: 1500, name: '删除', perms: 'ukey:delete', icon: 'el-icon-delete', type: 2, status: 1, orderNum: 1 },
      { id: 1503, parentId: 1500, name: '修改', perms: 'ukey:edit', icon: 'el-icon-edit', type: 2, status: 1, orderNum: 1 },
      { id: 1504, parentId: 1500, name: '冻结', perms: 'ukey:freeze', icon: 'el-icon-aim', type: 2, status: 1, orderNum: 1 },
      { id: 1505, parentId: 1500, name: '解冻', perms: 'ukey:unfreeze', icon: 'el-icon-top-left', type: 2, status: 1, orderNum: 1 }
    ]
  },
  {
    id: 1600,
    parentId: undefined,
    name: '日志审计',
    perms: 'log',
    url: 'log/index',
    icon: 'log',
    type: 1,
    status: 1,
    orderNum: 1
  }
]

export default [
  {
    url: '/menus/tree',
    type: 'get',
    response: config => {
      const { keyword, page = 1, limit = 10, sort } = config.query

      let mockList = menuData.filter(item => {
        if (keyword && item.loginName.indexOf(keyword) < 0 && item.userName.indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/menus/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const user of menuData) {
        if (user.id === +id) {
          return {
            code: 200,
            success: true,
            data: user
          }
        }
      }
    }
  },

  {
    url: '/menus',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/menus',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]
