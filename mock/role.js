import Mock from 'mockjs'
import { deepClone } from '../src/utils/index.js'
import { asyncRoutes, constantRoutes } from './routes.js'

import { menuData } from './menu.js'

const routes = deepClone([...constantRoutes, ...asyncRoutes])

// 递归循环获取id
const operationIdAry = []
function filterOperationMenuId(filterData) {
  filterData.forEach(item => {
    if (item.children) {
      filterOperationMenuId(item.children)
    }
    if (item.type === 2) {
      operationIdAry.push(item.id)
    }
  })
}
filterOperationMenuId(menuData)
// 随机打乱顺序
operationIdAry.sort(function() { return 0.5 - Math.random() })

const roles = [
  Mock.mock({
    id: '@increment',
    code: 'admin',
    name: '超级管理员',
    isDefault: 0,
    remark: '超级管理员，拥有所有的权限.',
    createTime: '@datetime',
    operId: '@cname',
    routes: routes,
    'permissions|10-40': [
      {
        'menuId|+1': operationIdAry
      }
    ]
  }),
  Mock.mock({
    id: '@increment',
    code: 'keyMgr',
    name: '密钥管理员',
    isDefault: 0,
    remark: '密钥管理员，密钥管理权限.',
    createTime: '@datetime',
    operId: '@cname',
    routes: routes.filter(i => i.path !== '/system'), // 普通用户，看不到系统管理
    'permissions|10-40': [
      {
        'menuId|+1': operationIdAry
      }
    ]
  }),
  Mock.mock({
    id: '@increment',
    code: 'general',
    name: '普通用户',
    isDefault: 1,
    remark: '普通用户，只有部分的权限.',
    createTime: '@datetime',
    operId: '@cname',
    routes: [{
      path: '',
      redirect: 'dashboard',
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          meta: { title: 'dashboard', icon: 'dashboard' }
        }
      ]
    }],
    'permissions|10-40': [
      {
        'menuId|+1': operationIdAry
      }
    ]
  })
]

for (let i = 0; i < 10; i++) {
  roles.push(Mock.mock({
    id: '@increment',
    code: '@word(3, 5)',
    name: '@cword(3,5)',
    isDefault: 0,
    remark: '',
    createTime: '@datetime',
    operId: '@cname',
    'permissions|10-40': [
      {
        'menuId|+1': operationIdAry
      }
    ]
  }))
}

export default [
  {
    url: '/roles/page',
    type: 'get',
    response: config => {
      const { keyword, page = 1, limit = 10, sort } = config.query

      let mockList = roles.filter(item => {
        if (keyword && item.code.indexOf(keyword) < 0 && item.name.indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = roles.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },
  {
    url: '/roles/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const role of roles) {
        if (role.id === +id) {
          return {
            code: 200,
            success: true,
            data: role
          }
        }
      }
    }
  },

  {
    url: '/roles',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/roles',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]
