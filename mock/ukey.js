import Mock from 'mockjs'

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    name: '@ctitle(3,5)',
    serialNumber: '@natural()',
    publicKeyValue: '@string(100)',
    state: '@integer(0, 1)',
    initOperId: '@cname',
    initOperTime: '@datetime',
    description: ''
  }))
}

export default [
  {
    url: '/ukeys/page',
    type: 'get',
    response: config => {
      const { keyword, state, page = 1, limit = 10, sort } = config.query

      let mockList = List.filter(item => {
        if (state && item.state !== parseInt(state)) return false
        if (keyword && item.name.indexOf(keyword) < 0 && item.serialNumber.toString().indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/ukeys/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const ukey of List) {
        if (ukey.id === +id) {
          return {
            code: 200,
            success: true,
            data: ukey
          }
        }
      }
    }
  },

  {
    url: '/ukeys',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/ukeys',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]

