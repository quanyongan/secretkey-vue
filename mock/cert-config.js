import Mock from 'mockjs'

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    caType: '@integer(0,1)',
    caName: '@pick(["部级CA", "第三方CA"])',
    raIp: '@ip()',
    raPort: '@integer(1000, 9999)',
    state: '@integer(0, 2)',
    remarks: '',
    applyOperid: '@cname',
    applyTime: '@datetime',
    approveOperid: '@cname',
    approveTime: '@datetime'
  }))
}

export default [
  {
    url: '/certConfigs/page',
    type: 'get',
    response: config => {
      const { caName, state, caType, page = 1, limit = 10, sort } = config.query

      let mockList = List.filter(item => {
        if (caType && item.caType !== parseInt(caType)) return false
        if (state && item.state !== parseInt(state)) return false
        if (caName && item.caName.indexOf(caName) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/certConfigs/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const certConfig of List) {
        if (certConfig.id === +id) {
          return {
            code: 200,
            success: true,
            data: certConfig
          }
        }
      }
    }
  },

  {
    url: '/certConfigs',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/certConfigs',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]

