import Mock from 'mockjs'

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    cerSerialNumber: '@natural()',
    terminalNumber: '@natural()',
    cerType: '@integer(0, 2)',
    cerAlgoType: '@integer(0, 1)',
    caName: '@pick(["部级CA", "第三方CA"])',
    raIp: '@ip()',
    raPort: '@integer(1000, 9999)',
    keyName: '@word(10)',
    cerData: '@string(100)',
    issueTime: '@date',
    validityTime: '@date',
    cerVersionNum: '@pick(["1.0.0", "2.2.11","8.9.3", "18.8.1","15.7.9"])',
    cerIssuer: '@name()',
    cerSignAlgo: '@pick(["RSA1024", "RSA2048","SM2", "密文存储", "采用SM3算法"])',
    state: '@integer(0, 2)',
    remarks: '',
    applyOperid: '@cname',
    applyTime: '@datetime',
    approveOperid: '@cname',
    approveTime: '@datetime'
  }))
}

export default [
  {
    url: '/certIssuances/page',
    type: 'get',
    response: config => {
      const { keyword, cerType, page = 1, limit = 10, sort } = config.query

      let mockList = List.filter(item => {
        if (cerType && item.cerType !== parseInt(cerType)) return false
        if (keyword && item.cerSerialNumber.toString().indexOf(keyword) < 0 && item.terminalNumber.toString().indexOf(keyword) < 0 && item.caName.indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/certIssuances/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const certIssuance of List) {
        if (certIssuance.id === +id) {
          return {
            code: 200,
            success: true,
            data: certIssuance
          }
        }
      }
    }
  },

  {
    url: '/certIssuances',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/certIssuances',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]

