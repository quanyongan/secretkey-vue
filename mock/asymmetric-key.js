import Mock from 'mockjs'

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    keyIden: '@pick(["粤通卡MF主控密钥", "ESAM的MF主控密钥"])',
    keyName: '@pick(["MF_MK", "MF_AK", "MF_BK"])',
    keyIndex: '@pick(["密文存储", "采用SM3算法"])',
    keyType: '非对称密钥',
    keyAlgo: '@integer(0, 1)',
    publicKeyValue: '@string(100)',
    keyCheckValue: '@string(16)',
    keyState: '@integer(0, 2)',
    intHsm: '@ip()',
    remarks: '',
    applyOperid: '@cname',
    applyTime: '@datetime',
    approveOperid: '@cname',
    approveTime: '@datetime'
  }))
}

export default [
  {
    url: '/asymmetricKeys/page',
    type: 'get',
    response: config => {
      const { keyName, keyState, page = 1, limit = 10, sort } = config.query

      let mockList = List.filter(item => {
        if (keyState && item.keyState !== parseInt(keyState)) return false
        if (keyName && item.keyName.indexOf(keyName) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/asymmetricKeys/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const asymmetricKey of List) {
        if (asymmetricKey.id === +id) {
          return {
            code: 200,
            success: true,
            data: asymmetricKey
          }
        }
      }
    }
  },

  {
    url: '/asymmetricKeys',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/asymmetricKeys',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]

