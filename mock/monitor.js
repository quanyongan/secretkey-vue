import Mock from 'mockjs'

const monitorData = [
  Mock.mock({
    id: '1000',
    cnName: '加密机管理系统',
    enName: 'encryption machine management system',
    ip: '@ip()',
    port: '@integer(1000, 9999)',
    status: '1',
    description: '',
    createBy: '@cname',
    createTime: '@datetime',
    updateBy: '@cname',
    updateTime: '@datetime',
    internalStorage: '@integer(200, 1000)',
    cpu: '@float(1, 5, 1, 1)'
  }),
  Mock.mock({
    id: '1001',
    cnName: '密钥管理系统',
    enName: 'Key management system',
    ip: '@ip()',
    port: '@integer(1000, 9999)',
    status: '1',
    description: '',
    createBy: '@cname',
    createTime: '@datetime',
    updateBy: '@cname',
    updateTime: '@datetime',
    internalStorage: '@integer(200, 1000)',
    cpu: '@float(1, 5, 1, 1)'
  }),
  Mock.mock({
    id: '1002',
    cnName: '证书管理系统',
    enName: 'certificate management system',
    ip: '@ip()',
    port: '@integer(1000, 9999)',
    status: '1',
    description: '',
    createBy: '@cname',
    createTime: '@datetime',
    updateBy: '@cname',
    updateTime: '@datetime',
    internalStorage: '@integer(200, 1000)',
    cpu: '@float(1, 5, 1, 1)'
  }),
  Mock.mock({
    id: '1003',
    cnName: 'UKEY管理系统',
    enName: 'ukey management system',
    ip: '@ip()',
    port: '@integer(1000, 9999)',
    status: '1',
    description: '',
    createBy: '@cname',
    createTime: '@datetime',
    updateBy: '@cname',
    updateTime: '@datetime',
    internalStorage: '@integer(200, 1000)',
    cpu: '@float(1, 5, 1, 1)'
  }),
  Mock.mock({
    id: '1004',
    cnName: '日志管理系统',
    enName: 'log management system',
    ip: '@ip()',
    port: '@integer(1000, 9999)',
    status: '1',
    description: '',
    createBy: '@cname',
    createTime: '@datetime',
    updateBy: '@cname',
    updateTime: '@datetime',
    internalStorage: '@integer(200, 1000)',
    cpu: '@float(1, 5, 1, 1)'
  })
]

export default [
  {
    url: '/monitors/page',
    type: 'get',
    response: config => {
      const { keyword, status, page = 1, limit = 10, sort } = config.query

      let mockList = monitorData.filter(item => {
        if (status && item.status !== parseInt(status)) return false
        if (keyword && item.cnName.indexOf(keyword) < 0 && item.enName.indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/monitors/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const monitor of monitorData) {
        if (monitor.id === +id) {
          return {
            code: 200,
            success: true,
            data: monitor
          }
        }
      }
    }
  },

  {
    url: '/monitors',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/monitors',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]

