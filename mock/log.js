import Mock from 'mockjs'

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    unitName: '@ctitle(3,5)',
    userName: '@cname()',
    ip: '@ip()',
    className: 'com.@domain().@capitalize(@word(5)Service',
    methodName: '@pick(["create", "update", "delete", "approve", "freeze","unfreeze"])',
    msg: '@integer(0, 1)',
    type: '@integer(0, 2)',
    createTime: '@datetime'
  }))
}

export default [
  {
    url: '/logs/page',
    type: 'get',
    response: config => {
      const { keyword, startDate, endDate, page = 1, limit = 10, sort } = config.query
      let mockList = List.filter(item => {
        if (startDate && endDate) {
          const dateTimeAry = item.createTime.split(' ')
          const date = dateTimeAry[0].replace(/-/g, '')
          if (parseInt(startDate) > parseInt(date) || parseInt(endDate) < parseInt(date)) return false
        }
        if (keyword && item.unitName.indexOf(keyword) < 0 && item.userName.toString().indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  }
]

