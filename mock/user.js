import Mock from 'mockjs'

const tokens = {
  admin: {
    token: 'admin-token'
  },
  editor: {
    token: 'editor-token'
  }
}

const List = []
const count = 100
for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    loginName: '@word(3, 5)',
    userName: '@cname',
    password: '111111',
    status: '@integer(0, 1)',
    createTime: '@datetime',
    updateTime: '@datetime',
    lastLoginTime: '@datetime',
    keySerial: '@natural()',
    keyPublickKey: '@string(100)',
    keyCer: '@string(55)',
    bindingStatus: '@integer(0, 1)',
    keyStatus: '@integer(0, 1)',
    type: '@integer(0, 1)',
    remarks: '',
    operId: '@cname',
    operTime: '@datetime'
  }))
}

const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://note.wiz.cn/as/user/avatar/cfedf583-7a20-4dbb-b831-6d5acf89d8a6?default=true&_1561087459154',
    name: 'Super Admin'
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

export default [
  // 登录
  {
    url: '/login',
    type: 'post',
    response: config => {
      const { username } = config.body
      const token = tokens[username]

      // mock error
      if (!token) {
        return {
          code: 60204,
          message: '用户名或者密码不正确.'
        }
      }

      return {
        code: 200,
        data: token
      }
    }
  },
  // 登出
  {
    url: '/logout',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        data: 'success'
      }
    }
  },
  {
    url: '/users/info\.*',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]
      // mock error
      if (!info) {
        return {
          code: 50008,
          message: 'Login failed, unable to get user details.'
        }
      }
      return {
        code: 200,
        data: info
      }
    }
  },
  {
    url: '/users/page',
    type: 'get',
    response: config => {
      const { keyword, page = 1, limit = 10, sort } = config.query

      let mockList = List.filter(item => {
        if (keyword && item.loginName.indexOf(keyword) < 0 && item.userName.indexOf(keyword) < 0) return false
        return true
      })
      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 200,
        success: true,
        data: {
          total: mockList.length,
          rows: pageList
        }
      }
    }
  },

  {
    url: '/users/[A-Za-z0-9]',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const user of List) {
        if (user.id === +id) {
          return {
            code: 200,
            success: true,
            data: user
          }
        }
      }
    }
  },

  {
    url: '/users',
    type: 'post',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  },

  {
    url: '/users',
    type: 'put',
    response: _ => {
      return {
        code: 200,
        success: true,
        data: 'success'
      }
    }
  }
]
