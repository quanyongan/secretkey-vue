import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/logs/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/logs/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/logs',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/logs',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/logs/' + id,
    method: 'delete'
  })
}
