import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/certIssuances/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/certIssuances/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/certIssuances',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/certIssuances',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/certIssuances/' + id,
    method: 'delete'
  })
}
