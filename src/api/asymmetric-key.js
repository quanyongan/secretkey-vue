import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/asymmetricKeys/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/asymmetricKeys/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/asymmetricKeys',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/asymmetricKeys',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/asymmetricKeys/' + id,
    method: 'delete'
  })
}
