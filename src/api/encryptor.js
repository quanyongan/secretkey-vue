import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/encryptors/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/encryptors/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/encryptors',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/encryptors',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/encryptors/' + id,
    method: 'delete'
  })
}
