import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/ukeys/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/ukeys/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/ukeys',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/ukeys',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/ukeys/' + id,
    method: 'delete'
  })
}
