import request from '@/utils/request'

// ------------- 登录登出--------------//
export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}
export function getInfo(token) {
  return request({
    url: '/users/info',
    method: 'get',
    params: { token }
  })
}

export function getPageData(query) {
  return request({
    url: '/users/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/users/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/users',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/users',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/users/' + id,
    method: 'delete'
  })
}
