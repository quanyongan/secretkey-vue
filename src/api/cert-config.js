import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/certConfigs/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/certConfigs/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/certConfigs',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/certConfigs',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/certConfigs/' + id,
    method: 'delete'
  })
}
