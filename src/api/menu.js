import request from '@/utils/request'

export function getTreeData(query) {
  return request({
    url: '/menus/tree',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/menus/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/menus',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/menus',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/menus/' + id,
    method: 'delete'
  })
}
