import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/monitors/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/monitors/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/monitors',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/monitors',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/monitors/' + id,
    method: 'delete'
  })
}
