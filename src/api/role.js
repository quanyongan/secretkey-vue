import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/roles/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/roles/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/roles',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/roles',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/roles/' + id,
    method: 'delete'
  })
}
