import request from '@/utils/request'

export function getPageData(query) {
  return request({
    url: '/symmetricKeys/page',
    method: 'get',
    params: query
  })
}

export function get(id) {
  return request({
    url: '/symmetricKeys/' + id,
    method: 'get',
    params: { id }
  })
}

export function create(data) {
  return request({
    url: '/symmetricKeys',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/symmetricKeys',
    method: 'put',
    data
  })
}

export function deleteById(id) {
  return request({
    url: '/symmetricKeys/' + id,
    method: 'delete'
  })
}
