import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'dashboard',
        meta: { title: '首页', icon: 'home', affix: true }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user', noCache: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/encryptor',
    component: Layout,
    redirect: '/encryptor/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/encryptor/index'),
        name: 'encryptor',
        meta: { title: '加密机管理', icon: 'encryptor' }
      }
    ]
  },
  {
    path: '/secretKey',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    name: 'secretKey',
    meta: {
      title: '密钥管理',
      icon: 'password'
    },
    children: [
      {
        path: 'symmetricKey',
        component: () => import('@/views/secret-key/symmetric-key'),
        name: 'symmetricKey',
        meta: {
          title: '对称密钥管理',
          icon: 'symmetricKey'
        }
      },
      {
        path: 'asymmetricKey',
        component: () => import('@/views/secret-key/asymmetric-key'),
        name: 'asymmetricKey',
        meta: {
          title: '非对称密钥管理',
          icon: 'asymmetricKey'
        }
      }
    ]
  },
  {
    path: '/certificate',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    name: 'certificate',
    meta: {
      title: '证书管理',
      icon: 'certificate'
    },
    children: [
      {
        path: 'certConfig',
        component: () => import('@/views/certificate/cert-config'),
        name: 'certConfig',
        meta: {
          title: '证书服务配置管理',
          icon: 'certConfig'
        }
      },
      {
        path: 'certIssuance',
        component: () => import('@/views/certificate/cert-issuance'),
        name: 'certIssuance',
        meta: {
          title: '证书签发管理',
          icon: 'certIssuance'
        }
      }
    ]
  },
  {
    path: '/monitor',
    component: Layout,
    redirect: '/monitor/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/monitor/index'),
        name: 'monitor',
        meta: { title: '监控管理', icon: 'monitor' }
      }
    ]
  },
  {
    path: '/system',
    component: Layout,
    alwaysShow: true, // will always show the root menu
    name: 'system',
    meta: {
      title: '系统管理',
      icon: 'system'
    },
    children: [
      {
        path: 'user',
        component: () => import('@/views/system/user'),
        name: 'user',
        meta: { title: '用户管理', icon: 'user' }
      },
      {
        path: 'role',
        component: () => import('@/views/system/role'),
        name: 'role',
        meta: { title: '角色管理', icon: 'role' }
      },
      {
        path: 'menu',
        component: () => import('@/views/system/menu'),
        name: 'menu',
        meta: { title: '菜单管理', icon: 'list' }
      }
    ]
  },
  {
    path: '/ukey',
    component: Layout,
    redirect: '/ukey/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/ukey/index'),
        name: 'ukey',
        meta: { title: 'UKEY管理', icon: 'ukey' }
      }
    ]
  },
  {
    path: '/log',
    component: Layout,
    redirect: '/log/index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/log/index'),
        name: 'log',
        meta: { title: '日志审计', icon: 'log' }
      }
    ]
  },
  /** when your routing map is too long, you can split it into small modules **/
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
